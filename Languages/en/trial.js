{
"trial_title" : "Title",
"trial_author" : "Author",
"trial_collabs" : "Collaborators",
"trial_playtesters" : "Playtesters",
"trial_metadata" : "Metadata",

"play" : "Play!",
"playtest" : "Playtest",
"edit" : "Edit",

"trial_sequence" : "Part of",
"trial_language" : "Language",
"trial_creation_date" : "Work started on",
"trial_released" : "Publicly released",
"trial_release_date" : "Released on",
"trial_add_collab" : "Add collaborator",
"trial_add_playtester" : "Add playtester",
"trial_delete": "Delete trial",
"trial_create": "Create a new trial",

"sequence_title": "Title",
"sequence_metadata": "Metadata",
"sequence_items": "Sequence items",
"sequence_append_trial": "Append a trial",
"sequence_delete": "Delete sequence",
"sequence_create": "Create a new sequence",

"trial_backups": "Trial file backups",
"backups_auto": "Automatic backups",
"backups_manual": "Manual backups",
"backups_manual_create": "Save current version as a new backup"
}
