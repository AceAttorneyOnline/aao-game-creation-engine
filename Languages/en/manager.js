{
"trial_manager" : "Trial manager",

"own_trials" : "Your trials",
"collaborating_trials" : "Trials you collaborate on",

"independant_trials" : "Independent trials",
"trial_sequences" : "Trial series",

"open_editor" : "Open in editor",
"save" : "Save",
"confirm_delete" : "Are you sure you wish to delete this element ? This operation is irreversible.",

"case_rules" : "I agree that I have read the <a href=\"<url>\">case rules</a>, and this case will follow them."
}
