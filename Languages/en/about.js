{
	"about_aao" : "About Ace Attorney Online",
	
	"aao_core_team" : "Core team",
	
	"aao_role_unas" : "Founder, Developer, Administrator",
	"aao_role_meph" : "English community manager, Designer",
	"aao_role_kroki" : "French community manager",
	"aao_role_daniel" : "Spanish community manager",
	"aao_role_thepasch" : "German community manager",
	
	"aao_supporting_teams" : "Supporting teams",
	
	"aao_mods" : "Forum moderators",
	"aao_qa_en" : "English trial reviewers",
	"aao_qa_fr" : "French trial reviewers",
	"aao_qa_es" : "Spanish trial reviewers",
	"aao_qa_de" : "German trial reviewers",
	
	"aao_special_thanks" : "Special thanks",
	
	"aao_role_spparrow" : "Founder, Original idea",
	"aao_role_devsupport" : "Development support",
	"aao_role_translation" : "Translation support",
	"aao_role_music" : "Original music composers",
	"aao_baotl_winner" : "Winner of \"The Bright Age of the Law\" competition"
}
