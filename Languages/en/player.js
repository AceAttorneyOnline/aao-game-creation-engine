{
"not_loaded" : "The trial could not be loaded",
"trial_edited_since_save" : "The trial has been edited since you saved your game. This might result in an inconsistent game.",
"trial_doesnt_match_save" : "The save you provided was not created on this trial.",
"tech_save_env_not_initialised" : "The saving environment was not initialised properly. Saving is disabled. Please report this issue on the forums and give a link to this page.",


"start" : "Start",
"loading_sounds" : "Loading sounds:",
"loading_images" : "Loading images:",
"end" : "The End",

"press" : "Press",
"present" : "Present",
"examine" : "Examine",
"move" : "Move",
"talk" : "Talk",

"back" : "Back",



"profiles" : "Profiles",
"evidence" : "Evidence",
"select" : "Select",
"check" : "Check",



"player_settings" : "Settings",
"mute" : "Mute",
"instant_text_typing" : "Instant text typing",

"player_saves" : "Game saves",
"save_new" : "New save",
"save_explain" : "Game saves are stored in your web browser. If you go to a different computer or if your browser's cache is cleared, you will lose your saves.\n\nTo avoid that, or to share saved games with other people, you can right click the save link, copy the link address, and store it in a safer place.",
"save_error_game_not_started": "You cannot save the game before starting it.",
"save_error_pending_timer": "You cannot save during a timed frame. Please wait.",
"save_error_frame_typing": "You cannot save while the text is typing. Please wait.",

"player_debug" : "Debugger",

"debug_status" : "Player status",
"frame_id" : "Frame ID#",
"frame_index" : "Frame index",

"debug_vars" : "Variables",
"add_var" : "Define a variable",
"var_name" : "Name of the variable",

"debug_cr" : "Court records",

"debug_scenes" : "Scenes",

"debug_frames" : "Frames",
"show_frame" : "Watch another frame",

"text_engine_missing_closing_tag" : "A closing tag for a text effect is missing. The game cannot continue."
}
