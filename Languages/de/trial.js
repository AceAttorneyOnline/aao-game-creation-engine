{
"trial_title" : "Titel",
"trial_author" : "Autor",
"trial_collabs" : "Mitwirkende",
"trial_playtesters" : "Tester",
"trial_metadata" : "Metadaten",

"play" : "Spielen!",
"playtest" : "Testen",
"edit" : "Bearbeiten",

"trial_sequence" : "Teil von",
"trial_language" : "Sprache",
"trial_creation_date" : "Kreiert am",
"trial_released" : "Öffentlich",
"trial_release_date" : "Released am",
"trial_add_collab" : "Mitwirkenden hinzufügen",
"trial_add_playtester" : "Tester hinzufügen",
"trial_delete": "Fall löschen",
"trial_create": "Neuen Fall erstellen",

"sequence_title": "Titel",
"sequence_metadata": "Metadaten",
"sequence_items": "Inhalte der Sequenz",
"sequence_append_trial": "Fall hinzufügen",
"sequence_delete": "Sequenz löschen",
"sequence_create": "Neue Sequenz erstellen",

"trial_backups": "Backups",
"backups_auto": "Automatische Backups",
"backups_manual": "Manuelle Backups",
"backups_manual_create": "Momentane Version als neues Backup speichern"
}
