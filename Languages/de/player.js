{
"not_loaded" : "Der Fall konnte nicht geladen werden",
"trial_edited_since_save" : "Der Fall wurde bearbeitet, seit Du dein Spiel gespeichert hast. Dies könnte zu Problemen im Gameplay führen.",
"trial_doesnt_match_save" : "Der Spielstand gehört nicht zu diesem Fall.",
"tech_save_env_not_initialised" : "Das Speichersystem konnte nicht korrekt initialisiert werden. Bitte melde Dich im Forum und hinterlasse einen Link zu dieser Seite.",


"start" : "Start",
"loading_sounds" : "Lade Sounds:",
"loading_images" : "Lade Bilder:",
"end" : "Ende",

"press" : "Angreifen",
"present" : "Präsentieren",
"examine" : "Untersuchen",
"move" : "Bewegen",
"talk" : "Reden",

"back" : "Zurück",



"profiles" : "Profile",
"evidence" : "Beweise",
"select" : "Auswählen",
"check" : "Überprüfen",



"player_settings" : "Einstellungen",
"mute" : "Stumm",
"instant_text_typing" : "Text sofort anzeigen",

"player_saves" : "Spielstände",
"save_new" : "Neuer Spielstand",
"save_explain" : "Spielstände werden in Deinem Browser gespeichert. Wenn Du dich auf einem anderen Computer anmeldest oder deinen Browser-Cache löschst, verlierst Du Deine Spelstände.\n\nUm das zu vermeiden, oder um Spielstände mit Freunden zu teilen, rechtsklicke auf den Link, kopiere die URL und speichere sie an einem sicheren Ort.",
"save_error_game_not_started": "Du kannst das Spiel nicht speichern, bevor es begonnen hat.",
"save_error_pending_timer": "Du kannst das Spiel nicht während eines zeitlich begrenzten Frames speichern. Bitte habe einen Augenblick Geduld.",
"save_error_frame_typing": "Du kannst nicht speichern, während der Text noch getippt wird. Bitte habe einen Augenblick Geduld.",

"player_debug" : "Debugger",

"debug_status" : "Status des Spielers",
"frame_id" : "Frame-ID#",
"frame_index" : "Frame-Index",

"debug_vars" : "Variablen",
"add_var" : "Variable definieren",
"var_name" : "Name der Variable",

"debug_cr" : "Gerichtsakte",

"debug_scenes" : "Szenen",

"debug_frames" : "Frame",
"show_frame" : "Zu Frame springen",

"text_engine_missing_closing_tag" : "Ein schließender Tag eines Texteffekts fehlt. Das Spiel kann nicht fortgesetzt werden."
}
