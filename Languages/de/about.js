{
	"about_aao" : "Über Ace Attorney Online",
	
	"aao_core_team" : "Hauptteam",
	
	"aao_role_unas" : "Gründer, Entwickler, Administrator",
	"aao_role_meph" : "Communitymanager der Englischen Sektion, Designer",
	"aao_role_kroki" : "Communitymanader der Französischen Sektion",
	"aao_role_daniel" : "Communitymanager der Spanischen Sektion",
	"aao_role_thepasch" : "Communitymanager der Deutschen Sektion",
	
	"aao_supporting_teams" : "Unterstützende Teams",
	
	"aao_mods" : "Forenmoderatoren",
	"aao_qa_en" : "Englische Fallprüfer",
	"aao_qa_fr" : "Französische Fallprüfer",
	"aao_qa_es" : "Spanische Fallprüfer",
	"aao_qa_de" : "Deutsche Fallprüfer",
	
	"aao_special_thanks" : "Besonderen Dank an",
	
	"aao_role_spparrow" : "Gründer, Originalidee",
	"aao_role_devsupport" : "Entwicklung",
	"aao_role_translation" : "Übersetzung",
	"aao_role_music" : "Komposition selbsterstellter Musik",
	"aao_baotl_winner" : "Sieger des \"The Bright Age of the Law\"-Wettbewerbs"
}
